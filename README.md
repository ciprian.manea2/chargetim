# ChargeTIM - Sistem de gestionare a stațiilor de încărcare electrică

### Adresa proiectului pe GitLab:

https://gitlab.upt.ro/ciprian.manea2/chargetim
___

### Aplicații și versiuni recomandate pentru deschiderea și rularea proiectului

- PHPStorm
- Node.js v12.0.0
- npm v6.9.0
- PHP v7.4
- Composer v2.0.0
___

### Instructiuni de compilare a proiectului

- Instaleaza Node.js v12.0.0 de pe site-ul oficial: https://nodejs.org/en
- Instaleaza PHP v7.4 consultând site-ul oficial: https://www.php.net/manual/en/install.php
- Instaleaza Composer v2.0.0 consultând site-ul oficial: https://getcomposer.org/download/
- Instalează postgreSQL consultând site-ul oficial: https://www.postgresql.org/download/
- Clonează clientul și serverul de pe linkul de gitlab: https://gitlab.upt.ro/ciprian.manea2/chargetim
- Rulează `npm install` pe client pentru a instala toate dependințele necesare proiectului
- Ruleaza `npm start` pe client pentru a porni proiectul
- Rulează `composer install` pe server pentru a instala toate dependințele necesare proiectului
- Consulta documentația oficiala pentur a porni serverul pentru baza de date pentru sistemul de oferare folosit: https://www.postgresql.org/docs/current/server-start.html
- Rulează `php -S localhost:8000 -t public` pentru a porni proiectul
- Rulează `php artisan migrate` pentru a creea tabelele in baza de date
- Acceseaza: http://localhost:3000/ pentru a deschide proiectul

### Documentație

- Linkul pentru documentație: https://docs.google.com/document/d/1t_-m6RlE2OLYSIPggWAFeDYSLaeZcLMg/edit?usp=sharing&ouid=113490083874824282207&rtpof=true&sd=true
